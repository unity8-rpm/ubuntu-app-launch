%define sover 3
%define soname libubuntu-app-launch
%define debug_package %{nil}


Name:		ubuntu-app-launch
Version:	r3022
Release:	1%{?dist}
Summary:	Session init system job for Launching Applications

Group:		Applications
License:	GPL
Source0:	https://github.com/abhishek9650/ubuntu-app-launch/archive/%{name}-%{version}.tar.gz
Patch0:		FormatTruncation.patch
Patch1:		Gtest.patch


BuildRequires:	cmake, cmake-extras, dbus-test-runner, gmock-devel, properties-cpp-devel, lttng-ust-devel, redhat-lsb-core, gtest-devel, curl-devel
BuildRequires:	pkgconfig(gobject-introspection-1.0)
BuildRequires:	pkgconfig(json-glib-1.0)
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(gobject-2.0)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(zeitgeist-2.0)
BuildRequires:	pkgconfig(dbus-1)
BuildRequires:	pkgconfig(dbustest-1)
BuildRequires:	pkgconfig(mirclient)
BuildRequires:	pkgconfig(libertine)
BuildRequires:	pkgconfig(libunity-api)
Requires:	zeitgeist, gobject-introspection, lttng-ust, mir, unity-api, properties-cpp-devel
Provides:	%{name}

%description
Session init system job for Launching Applications.


%package -n %{soname}-%{sover}
Summary:	C++ libraries provided by ubuntu app launch
Group:		System/Libraries

%description -n %{soname}-%{sover}
Header files provided by ubuntu-app-launch
This package provides shared libraries for ubuntu-app-launch


%package devel
Summary:	C++ header files provided by ubuntu-app-launch
Group:		Development/Libraries
Requires:	%{soname} = %{version}

%description devel
C++ Header files provided by ubuntu-app-launch
This package provides development libraries for ubuntu-app-launch


%package -n typelib-%{name}-3
Summary:	Session init system job for Launching Applications -- Introspection bindings
Group:		System/Libraries

%description -n typelib-%{name}-3
Session init system job for Launching Applications
This package provides GoObject Introspection bindings for ubuntu-app-launch


%prep
%setup -q
%patch0 -p1
%patch1 -p1
truncate -s 0 tests/CMakeLists.txt



%build
%cmake	-DCMAKE_INSTALL_PREFIX:PATH=/usr  \
	-DCMAKE_INSTALL_LIBDIR=%{_libdir}  \
	-DCMAKE_INSTALL_LIBEXECDIR=%{_libexecdir}
make %{?_smp_mflags}


#Tests are broken af
#%check
#make ARGS+="--output-on-failure" test


%install
%make_install


%clean
rm -rf $RPM_BUILD_ROOT


%post -n %{soname}-%{sover} -p /sbin/ldconfig

%postun -n %{soname}-%{sover} -p /sbin/ldconfig

%files
%doc COPYING
%{_bindir}/snappy-xmir
%{_bindir}/snappy-xmir-envvars
%{_bindir}/ubuntu-app-info
%{_bindir}/ubuntu-app-launch
%{_bindir}/ubuntu-app-launch-appids
%{_bindir}/ubuntu-app-list
%{_bindir}/ubuntu-app-list-pids
%{_bindir}/ubuntu-app-pid
%{_bindir}/ubuntu-app-stop
%{_bindir}/ubuntu-app-triplet
%{_bindir}/ubuntu-app-usage
%{_bindir}/ubuntu-app-watch
%{_bindir}/ubuntu-helper-list
%{_bindir}/ubuntu-helper-start
%{_bindir}/ubuntu-helper-stop
%{_bindir}/app-test/ubuntu-app-test
%{_datadir}/applications/ubuntu-app-test.desktop
%{_datadir}/icons/hicolor/scalable/apps/ubuntu-app-test.svg
%{_datadir}/%{name}/ubuntu-app-test.qml
%{_libexecdir}/ubuntu-app-launch/oom-adjust-setuid-helper
%{_libexecdir}/ubuntu-app-launch/socket-demangler
%{_libexecdir}/ubuntu-app-launch/systemd-helper-helper
%{_libexecdir}/ubuntu-app-launch/xmir-helper

%files -n %{soname}-%{sover}
%{_libdir}/libubuntu-app-launch.so
%{_libdir}/libubuntu-app-launch.so.*

%files -n typelib-ubuntu-app-launch-3
%{_datadir}/gir-1.0/UbuntuAppLaunch-3.gir
%{_libdir}/girepository-1.0/UbuntuAppLaunch-3.typelib


%files devel
%{_libdir}/pkgconfig/ubuntu-app-launch-3.pc
%{_includedir}/%{soname}-%{sover}/%{name}.h
%{_includedir}/%{soname}-%{sover}/%{name}/*.h


%changelog

